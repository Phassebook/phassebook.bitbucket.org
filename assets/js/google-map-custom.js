


// ******************************************************************************************
// Configuration Start		
// ******************************************************************************************

//*********************Google MAP *********************/

		var color = "#a33722"; // google map background colour
		var saturation = 100; //
		var mapLatitude=57.045724;
		var mapLongitude=9.951162;//(Fist Value Latitude, Second Value ), get YOUR coordenates here!: http://itouchmap.com/latlong.html
		var mapZoom_value=16; // Map zoom level parameter only numeric


// Google map marker example 2 locations 
		//map-marker #1
		var marker1_Latitude=57.045624;
		var marker1_Longitude=9.943206;
		var marker1_content="<h2>Pizza Huset</h2>"; // marker or  on click content (Info Window)
		var marker1_pointerUrl = 'assets/img/map-marker.png'; // set your color pointer here!

//********************* Google MAP END *********************/



//****************************************************************************
		  		// Google map 
//****************************************************************************
			//dragable mobile
			var drag;
			if($(window).width()<796){drag=false;}else{drag=true;}
			
		/* googleMaps */
		
				function map_canvas_loaded() {
				var latlng = new google.maps.LatLng(mapLatitude,mapLongitude);
				
				// setting styles here 
				var styles = [
					{
						"featureType": "landscape",
						"stylers": [
							{
								"saturation": -100
							},
							{
								"lightness": 65
							},
							{
								"visibility": "on"
							}
						]
					},
					{
						"featureType": "poi",
						"stylers": [
							{
								"saturation": -100
							},
							{
								"lightness": 51
							},
							{
								"visibility": "simplified"
							}
						]
					},
					{
						"featureType": "road.highway",
						"stylers": [
							{
								"saturation": -100
							},
							{
								"visibility": "simplified"
							}
						]
					},
					{
						"featureType": "road.arterial",
						"stylers": [
							{
								"saturation": -100
							},
							{
								"lightness": 30
							},
							{
								"visibility": "on"
							}
						]
					},
					{
						"featureType": "road.local",
						"stylers": [
							{
								"saturation": -100
							},
							{
								"lightness": 40
							},
							{
								"visibility": "on"
							}
						]
					},
					{
						"featureType": "transit",
						"stylers": [
							{
								"saturation": -100
							},
							{
								"visibility": "simplified"
							}
						]
					},
					{
						"featureType": "administrative.province",
						"stylers": [
							{
								"visibility": "off"
							}
						]
					},
					{
						"featureType": "water",
						"elementType": "labels",
						"stylers": [
							{
								"visibility": "on"
							},
							{
								"lightness": -25
							},
							{
								"saturation": -100
							}
						]
					},
					{
						"featureType": "water",
						"elementType": "geometry",
						"stylers": [
							{
								"hue": "#ffff00"
							},
							{
								"lightness": -25
							},
							{
								"saturation": -97
							}
						]
					}
				];
				var options = {
				 center : latlng,
				 mapTypeId: google.maps.MapTypeId.ROADMAP,
				 zoom : mapZoom_value,
				 styles: styles
				};
				var map_canvas = new google.maps.Map(document.getElementById('map_canvas'), options);
				
			
			
				
				//****************************************************************************
		  		// marker 1 content 
				//****************************************************************************
				var pointer1 = new google.maps.LatLng(marker1_Latitude,marker1_Longitude);
				
				var marker1= new google.maps.Marker({
				 position : pointer1,
				 map : map_canvas,
				 icon: marker1_pointerUrl //Custom Pointer URL
				 });
				
				google.maps.event.addListener(marker1,'click',
				 function() {
				 var infowindow = new google.maps.InfoWindow(
				 {content:marker1_content });
				 infowindow.open(map_canvas,marker1);
				 });
				// marker 1 END


				}

				window.onload = function() {
				 map_canvas_loaded();
				};
			/* End */
			

//Google map end 